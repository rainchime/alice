#!/usr/bin/env ruby

def complete!
  puts Alice.new.complete(*ARGV)
end

def run!
  a = Alice.new
  a.interpret(*ARGV)
end

class Alice

  def initialize
    load_pieces!
  end

  def complete *args
    _, current, last = *args
    if last == 'alice'
      return vocabulary.select { |v| v.start_with? current }.join("\n")
    end

    if ['describe', 'make', 'unmake', 'check'].include? last
      return pieces.keys.select { |v| v.start_with? current }.join("\n")
    end

    if last == 'list'
      return ['everything', 'made', 'unmade'].select { |v| v.start_with? current }.join("\n")
    end

    return
  end

  def vocabulary
    ['describe', 'make', 'unmake', 'check', 'help', 'list']
  end

  def interpret command, *args
    return help if !vocabulary.include? command
    
    if !['help', 'list'].include?(command)
      name = args[0]
      if !pieces[name]
        puts "i couldn't find a piece called #{name}. i'm sorry."
        return
      end
    end

    if vocabulary.include? command
      public_send(command.to_sym, *args)
    else
      help
    end
  end

  def list category
    if category == 'everything'
      puts "here's all the pieces i know: \n\n"
      pieces.each { |k, p| puts "  #{k}" }
      return
    end

    if category == 'made'
      puts "all these pieces have been made: \n\n"
      pieces.select { |k, p| p.check }.each { |k, p| puts "  #{k}" }
      return
    end

    if category == 'unmade'
      puts "none of these pieces have been made (yet): \n\n"
      pieces.select { |k, p| !p.check }.each { |k, p| puts "  #{k}" }
      return
    end
  end

  def describe name
    puts "#{name} is #{pieces[name].describe}"
  end

  def make name
    if pieces[name].check
      puts "i already made #{name}! or someone did, anyway."
      return
    end

    puts complies.sample
    puts ''
    pieces[name].make
    puts ''
    puts successes.sample
  end

  def unmake name
    if !pieces[name].check
      puts "#{name} is already gone, i think."
      return
    end

    puts complies.sample
    puts ''
    pieces[name].unmake
    puts ''
    puts successes.sample
  end

  def check name
    if (pieces[name].check)
      puts "#{name} is installed."
    else
      puts "#{name} is not installed."
    end
  end

  def help
    puts "hello, my name is alice. you can say things like:"
    puts "\n  alice check #{pieces.keys.sample}"
    puts "  alice describe #{pieces.keys.sample}"
    puts "  alice make #{pieces.keys.sample}"
    puts "  alice unmake #{pieces.keys.sample}"
    puts "\nand i will do my best to help."
  end

  private def complies
    ['okay.', 'sure.', 'will do.', 'can do.', 'alright.']
  end

  private def successes
    ['done.', 'it\'s done', 'i did it.', 'all done.', 'mission complete.']
  end

  private def pieces
    @pieces ||= load_pieces!
  end

  private def load_pieces!
    @pieces = {}
    Dir[File.join(__dir__, 'pieces', '*')].each do |file|
      require_relative file
      name = file.match(/pieces\/(.+)\.rb/)&.[](1)
      throw "name could not be parsed for #{file}" unless name

      piece = Object.const_get(name.capitalize).new
      @pieces[piece.name] = piece
    end
    @pieces
  end

end

if ENV['COMP_LINE']
  complete!
  exit
end

if __FILE__ == $0
  run!
  exit
end
