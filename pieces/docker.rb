
class Docker

  def name
    'docker'
  end

  def describe
    'a containerization... platform? framework? ...dock?'
  end

  def check
    `which docker > /dev/null 2>&1`
    return $? == 0
  end

  def make
    puts `yes | sudo pacman -S docker`
    puts `systemctl start docker.service`
    puts `systemctl enable docker.service`
    puts `sudo usermod -aG docker $USER`
  end

  def unmake
    puts `yes | sudo pacman -R docker`
  end

end

