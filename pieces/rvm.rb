
class Rvm

  def name
    'rvm'
  end

  def describe
    'a ruby version manager. the only one that matters, really.'
  end

  def check
    `which rvm > /dev/null 2>&1`
    return $? == 0
  end

  def make
    puts `gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB`
    puts `curl -sSL https://get.rvm.io | bash -s stable`
  end

  def unmake
    puts `yes yes | rvm implode`
  end

end

