
class Alacritty

  def name
    'alacritty'
  end

  def describe
    'a really fast terminal emulator, written in rust. its logo is a big \'A\' for Alice!'
  end

  def check
    `which alacritty > /dev/null 2>&1`
    return $? == 0
  end

  def make
    puts `yes | sudo pacman -S alacritty`
  end

  def unmake
    puts `yes | sudo pacman -R alacritty`
  end

end

