
class Dockercompose

  def name
    'docker-compose'
  end

  def describe
    'the thing you REALLY need if you want to use docker.'
  end

  def check
    `which docker-compose > /dev/null 2>&1`
    return $? == 0
  end

  def make
    puts `yes | sudo pacman -S docker-compose`
  end

  def unmake
    puts `yes | sudo pacman -R docker-compose`
  end

end

